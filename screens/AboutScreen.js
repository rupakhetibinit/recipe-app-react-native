import React from 'react';
import { View, Text, Button } from 'react-native';

const AboutScreen = ({ navigation }) => {
	return (
		<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
			<Text>About</Text>
			<Button
				title='Click to go to Home'
				onPress={() => {
					navigation.goBack();
				}}
			/>
		</View>
	);
};

export default AboutScreen;
