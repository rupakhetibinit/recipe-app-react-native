import React, { useState } from 'react';
import {
	View,
	Text,
	Button,
	StyleSheet,
	TouchableOpacity,
	Dimensions,
	TextInput,
	StatusBar,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';

const LoginScreen = ({ navigation }) => {
	const [data, setData] = useState({
		email: '',
		password: '',
		checkTextInputChange: false,
		secureTextEntry: true,
	});

	const textInputChange = (val) => {
		if (val.lenth !== 0) {
			setData({
				...data,
				email: val,
				checkTextInputChange: true,
			});
		} else {
			setData({
				...data,
				email: val,
				checkTextInputChange: false,
			});
		}
	};

	const handlePasswordChange = (val) => {
		setData({
			...data,
			password: val,
		});
	};
	const updateSecureTextEntry = () => {
		setData({
			...data,
			secureTextEntry: !data.secureTextEntry,
		});
	};

	return (
		<View style={styles.container}>
			<StatusBar backgroundColor='#009387' barStyle='light-content' />
			<View style={styles.header}>
				<Text style={styles.textHeader}>Welcome Back!</Text>
			</View>

			<Animatable.View animation='fadeInUpBig' style={styles.footer}>
				<View style={styles.footer}>
					<Text style={styles.textFooter}>Email</Text>
					<View style={styles.action}>
						<FontAwesome name='user-o' color='#05375a' size={20} />
						<TextInput
							placeholder='Your Email'
							style={styles.textInput}
							autoCapitalize='none'
							onChangeText={(val) => textInputChange(val)}
						/>
						{data.checkTextInputChange ? (
							<Animatable.View animation='bounceIn'>
								<Feather name='check-circle' color='green' size={20} />
							</Animatable.View>
						) : null}
					</View>
					<Text style={[styles.textFooter, { marginTop: 35 }]}>Password</Text>
					<View style={styles.action}>
						<FontAwesome name='lock' color='#05375a' size={20} />
						<TextInput
							placeholder='Your Password'
							style={styles.textInput}
							autoCapitalize='none'
							secureTextEntry={data.secureTextEntry ? true : false}
							onChangeText={(val) => {
								handlePasswordChange(val);
							}}
						/>
						<TouchableOpacity onPress={updateSecureTextEntry}>
							{data.secureTextEntry ? (
								<Feather name='eye-off' color='green' size={20} />
							) : (
								<Feather name='eye' color='green' size={20} />
							)}
						</TouchableOpacity>
					</View>
					<View style={styles.button}>
						<LinearGradient
							colors={['#08d4c4', '#01ab9d']}
							style={[styles.signIn, { color: '#fff' }]}
						>
							<Text>Login</Text>
						</LinearGradient>
						<TouchableOpacity
							onPress={() => navigation.navigate('Register')}
							style={[
								styles.signIn,
								{
									borderColor: '#009387',
									borderWidth: 1,
									marginTop: 15,
								},
							]}
						>
							<Text style={[styles.textSign, { color: '#009397' }]}>
								Register
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Animatable.View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#009387',
	},
	header: {
		flex: 2,
		justifyContent: 'flex-end',
		paddingHorizontal: 20,
		paddingBottom: 50,
	},
	footer: {
		flex: 3,
		backgroundColor: '#fff',
		borderTopRightRadius: 30,
		borderTopLeftRadius: 30,
		paddingHorizontal: 20,
		paddingVertical: 30,
	},
	textHeader: {
		color: '#fff',
		fontWeight: 'bold',
		fontSize: 30,
	},
	textFooter: {
		color: '#05375a',
		fontSize: 18,
	},
	action: {
		flexDirection: 'row',
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: '#f2f2f2',
		paddingBottom: 5,
	},
	textInput: {
		flex: 1,
		marginTop: -12,
		paddingLeft: 10,
		color: '#05375a',
	},
	button: {
		alignItems: 'center',
		marginTop: 50,
	},
	signIn: {
		width: 200,
		height: 50,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 10,
	},
	textSign: {
		fontSize: 18,
		fontWeight: 'bold',
	},
});

export default LoginScreen;
