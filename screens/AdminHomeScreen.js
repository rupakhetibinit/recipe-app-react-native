import React from 'react';
import { View, Text } from 'react-native';

const AdminHomeScreen = () => {
	return (
		<View>
			<Text>Hello from admin home screen</Text>
		</View>
	);
};

export default AdminHomeScreen;
