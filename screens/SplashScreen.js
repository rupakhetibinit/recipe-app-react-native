import React from 'react';
import * as Animatable from 'react-native-animatable';
import {
	View,
	Text,
	TouchableOpacity,
	Image,
	StyleSheet,
	Dimensions,
} from 'react-native';
import { useTheme } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialIcons } from '@expo/vector-icons';

const SplashScreen = ({ navigation }) => {
	const { colors } = useTheme();
	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<Image
					source={require('../assets/shopping.png')}
					style={styles.logo}
					resizeMode='contain'
				/>
			</View>
			<Animatable.View
				style={[
					styles.footer,
					{
						backgroundColor: colors.background,
					},
				]}
				animation='fadeInUpBig'
			>
				<Text
					style={[
						styles.title,
						{
							color: colors.text,
						},
					]}
				>
					Get Recipes at your doorstep
				</Text>
				<Text style={styles.text}>Login or Register Now</Text>
				<View style={styles.button}>
					<TouchableOpacity onPress={() => navigation.navigate('Login')}>
						<LinearGradient
							colors={['#2c6bed', '#2c6bed']}
							style={styles.logIn}
						>
							<Text style={styles.textSign}>Login</Text>
							<MaterialIcons name='navigate-next' color='#000' size={20} />
						</LinearGradient>
					</TouchableOpacity>
				</View>
				<View style={styles.button}>
					<TouchableOpacity onPress={() => navigation.navigate('Register')}>
						<LinearGradient
							colors={['#08d4c4', '#01ab9d']}
							style={styles.logIn}
						>
							<Text style={styles.textSign}>Register</Text>
							<MaterialIcons name='navigate-next' color='#000' size={20} />
						</LinearGradient>
					</TouchableOpacity>
				</View>
			</Animatable.View>
		</View>
	);
};

const { height } = Dimensions.get('screen');
const logoHeight = height * 0.45;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	header: {
		flex: 2,
		justifyContent: 'center',
		alignItems: 'center',
	},
	footer: {
		flex: 1,
		backgroundColor: '#fff',
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingVertical: 50,
		paddingHorizontal: 30,
	},
	logo: {
		width: logoHeight,
		height: logoHeight,
	},
	title: {
		color: '#2c6bed',
		fontSize: 30,
		fontWeight: 'bold',
	},
	text: {
		color: 'grey',
		marginTop: 5,
	},
	button: {
		alignItems: 'flex-end',
		marginTop: 30,
	},
	logIn: {
		width: 150,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 30,
		flexDirection: 'row',
	},
	textSign: {
		color: 'white',
		fontWeight: 'bold',
	},
});

export default SplashScreen;
