import React from 'react';
import { View, Text, Button } from 'react-native';

const HomeScreen = ({ navigation }) => {
	return (
		<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
			<Text>Home Screen</Text>
			<Button
				title='Click to go to about'
				onPress={() => {
					navigation.navigate('About');
				}}
			/>
		</View>
	);
};

export default HomeScreen;
