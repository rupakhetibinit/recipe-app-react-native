import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from '../screens/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen';
import AboutScreen from '../screens/AboutScreen';
import { View } from 'react-native-animatable';
import { FontAwesome5 } from '@expo/vector-icons';
import FavoritesScreen from '../screens/FavoritesScreen';

const UserNavigation = () => {
	const Tab = createBottomTabNavigator();
	return (
		<>
			<NavigationContainer>
				<Tab.Navigator
					initialRouteName='Profile'
					screenOptions={{
						tabBarShowLabel: false,
						// Floating Tab Bar....
						tabBarStyle: {
							backgroundColor: 'white',
							position: 'absolute',
							bottom: 40,
							marginHorizontal: 20,
							// Max Height....
							height: 60,
							borderRadius: 10,
							// Shadow...
							shadowColor: '#000',
							shadowOpacity: 0.06,
							shadowOffset: {
								width: 10,
								height: 10,
							},
						},
					}}
				>
					{
						// Tab Screens...
						// Tab Icons...
					}
					<Tab.Screen
						name='Home'
						component={HomeScreen}
						options={{
							tabBarIcon: ({ focused }) => {
								return (
									<View
										style={{
											position: 'absolute',
										}}
									>
										<FontAwesome5
											name='home'
											size={20}
											color={focused ? 'red' : 'gray'}
										/>
									</View>
								);
							},
						}}
					/>
					<Tab.Screen
						name='Favorites'
						component={FavoritesScreen}
						options={{
							tabBarIcon: ({ focused }) => {
								return (
									<View
										style={{
											position: 'absolute',
										}}
									>
										<FontAwesome5
											name='heart'
											size={20}
											color={focused ? 'red' : 'gray'}
										/>
									</View>
								);
							},
						}}
					/>

					<Tab.Screen
						name='Profile'
						component={ProfileScreen}
						options={{
							tabBarIcon: ({ focused }) => {
								return (
									<View
										style={{
											position: 'absolute',
										}}
									>
										<FontAwesome5
											name='user-alt'
											size={20}
											color={focused ? 'red' : 'gray'}
										/>
									</View>
								);
							},
						}}
					/>
					<Tab.Screen
						name='About'
						component={AboutScreen}
						options={{
							tabBarIcon: ({ focused }) => {
								return (
									<View
										style={{
											position: 'absolute',
										}}
									>
										<FontAwesome5
											name='hamburger'
											size={20}
											color={focused ? 'red' : 'gray'}
										/>
									</View>
								);
							},
						}}
					/>
				</Tab.Navigator>
			</NavigationContainer>
		</>
	);
};

export default UserNavigation;
