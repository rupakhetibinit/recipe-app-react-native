import React from 'react';
import AdminHomeScreen from '../screens/AdminHomeScreen';
import AdminOrdersScreen from '../screens/AdminOrdersScreen';
import { NavigationContainer } from '@react-navigation/native';

const AdminNavigation = () => {
	const Tab = createBottomTabNavigator();
	return (
		<>
			<NavigationContainer>
				<Tab.Navigator initialRouteName='AdminHomeScreen'>
					<Tab.Screen name='AdminHomeScreen' component={AdminHomeScreen} />
					<Tab.Screen name='AdminOrdersScreen' component={AdminOrdersScreen} />
				</Tab.Navigator>
			</NavigationContainer>
		</>
	);
};

export default AdminNavigation;
