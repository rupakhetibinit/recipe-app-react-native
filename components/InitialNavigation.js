import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';

const InitialNavigation = ({ globalScreenOptions }) => {
	const Stack = createNativeStackNavigator();
	return (
		<>
			<NavigationContainer>
				<Stack.Navigator
					screenOptions={globalScreenOptions}
					initialRouteName='Splash'
				>
					<Stack.Screen name='Splash' component={SplashScreen} />
					<Stack.Screen name='Login' component={LoginScreen} />
					<Stack.Screen name='Register' component={RegisterScreen} />
				</Stack.Navigator>
			</NavigationContainer>
		</>
	);
};

export default InitialNavigation;
