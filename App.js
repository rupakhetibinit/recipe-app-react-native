import React, { useState } from 'react';

import AdminNavigation from './components/AdminNavigation';
import UserNavigation from './components/UserNavigation';
import InitialNavigation from './components/InitialNavigation';

export default function App() {
	const [token, setToken] = useState(false);
	const [isAdmin, setIsAdmin] = useState(false);
	const globalScreenOptions = {
		headerShown: false,
	};
	if (!token) {
		return <InitialNavigation globalScreenOptions={globalScreenOptions} />;
	} else if (token && isAdmin) {
		return <AdminNavigation globalScreenOptions={globalScreenOptions} />;
	}
	return <UserNavigation globalScreenOptions={globalScreenOptions} />;
}
